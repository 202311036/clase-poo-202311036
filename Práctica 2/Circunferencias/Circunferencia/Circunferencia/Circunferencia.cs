﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circunferencia
{
    class Circunferencia
    {
        public double radio = 0;

        public void setRadio(double radio)
        {
            this.radio = radio;
        }

        public double getRadio()
        {
            return this.radio;
        }

        public void CalcularArea()
        {
            double Area = 0;
            Area = (radio * radio) * (3.14);
            Console.WriteLine("El valor del area es " + Area);
        }

        public void CalcularPerimetro()
        {
            double Perimetro = 0;
            Perimetro = (radio + radio) * (3.14);
            Console.WriteLine("El valor del perimetro es " + Perimetro);
        }
    }
}
