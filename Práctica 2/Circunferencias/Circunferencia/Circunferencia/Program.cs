﻿using System;

namespace Circunferencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Circunferencia Rueda = new Circunferencia();
            Circunferencia Moneda = new Circunferencia();

            Rueda.setRadio(10);
            double radioRueda = Rueda.getRadio();

            Moneda.setRadio(1);
            double radioMoneda = Moneda.getRadio();

            Rueda.CalcularArea();

            Moneda.CalcularPerimetro();

            Rueda.CalcularArea();

            Moneda.CalcularPerimetro();

            Console.ReadKey();
        }
    }
}
